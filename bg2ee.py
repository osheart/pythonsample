from PIL import ImageGrab
from pynput import keyboard
from pynput.mouse import Button, Controller
import time

# en janky måde at optimere sine stats i bg2

mouse = Controller()

x = 3073
#x = 3060
xdelta = 17
y = 923
#y = 928
ydelta = 15
mx = 3137
my = 1000

pics = []
count = []

def on_press(key):
    if hasattr(key, 'char'):
        if key.char == 'j':
            while True:
                mouse.position = (mx, my)
                mouse.click(Button.left, 1)
                time.sleep(4 / 60)
                pic = ImageGrab.grab(bbox=(x, y, x+xdelta, y + ydelta))
                try:
                    count[pics.index(pic)] += 1
                except:
                    pics.append(pic)
                    count.append(1)
                    break
        else:
            return False

# Collect events until released
with keyboard.Listener(on_press=on_press) as listener:
    listener.join()