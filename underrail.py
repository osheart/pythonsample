import pytesseract
import PIL
import cv2
import numpy as np
from PIL import Image, ImageGrab
from scipy.io import wavfile

# et script hvor jeg prøvede at bruge AI til at løse en udfordring i computerspillet underrail

#x = 2400
#y = 930
#dx = 900
#dy = 500

img = PIL.ImageGrab.grab()
#text  = pytesseract.image_to_string( image )

#text = text.strip()
#if ( len( text ) > 0 ):
#    print( text )

#img = pyautogui.screenshot()
img = cv2.cvtColor(np.array(img), cv2.COLOR_RGB2BGR)
#img = cv2.imread('sample.jpg')

def captch_ex(img):
    blue, green, red = cv2.split(img)
    green *= (blue<100) & (red<100) # filter out non-green text
    ret, mask = cv2.threshold(green, 180, 255, cv2.THRESH_BINARY)
    image_final = cv2.bitwise_and(green, green, mask=mask)
    ret, new_img = cv2.threshold(image_final, 180, 255, cv2.THRESH_BINARY)  # for black text , cv.THRESH_BINARY_INV
    kernel = cv2.getStructuringElement(cv2.MORPH_CROSS, (3,3))  # to manipulate the orientation of dilution , large x means horizonatally dilating  more, large y means vertically dilating more
    dilated = cv2.dilate(new_img, kernel, iterations=20)  # dilate , more the iteration more the dilation
    contours, hierarchy = cv2.findContours(dilated, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE)  # findContours returns 3 variables for getting contours

    for contour in contours:
        # get rectangle bounding contour
        [x, y, w, h] = cv2.boundingRect(contour)

        # Don't plot small false positives that aren't text
        if w < 35 and h < 35:
            continue

        # draw rectangle around contour on original image
        cv2.rectangle(img, (x, y), (x + w, y + h), (255, 0, 255), 2)

        text = pytesseract.image_to_string(green[y :y +  h , x : x + w])
        text = text.strip()
        if ( len( text ) > 0 ):
            print( text )

    # write original image with added contours to disk
    #cv2.imshow('captcha_result', img)
    #cv2.waitKey()

captch_ex(img)

#samplerate, data = wavfile.read("250Hz_44100Hz_16bit_05sec.wav")

def extract_peak_frequency(data, samplerate):
    fft_data = np.fft.fft(data)
    freqs = np.fft.fftfreq(len(data))

    peak_coefficient = np.argmax(np.abs(fft_data))
    peak_freq = freqs[peak_coefficient]

    return abs(peak_freq * samplerate)

#print(extract_peak_frequency(data,samplerate))
